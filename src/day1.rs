use std::collections::BinaryHeap;

pub fn ex1(lines: &Vec<String>) -> usize {
  let calorie_counts = get_calorie_values(lines);
  *calorie_counts.peek().unwrap()
}

pub fn ex2(lines: &Vec<String>) -> usize {
  let mut calorie_counts = get_calorie_values(lines);
  let mut sum = 0;
  for _ in 0..3 {
    sum += calorie_counts.pop().unwrap();
  }
  sum
}

fn get_calorie_values(lines: &Vec<String>) -> BinaryHeap<usize> {
  let numbers = lines.iter().map(|x| x.parse::<usize>().ok()).collect::<Vec<_>>();
  let mut heap = BinaryHeap::new();
  numbers
    .split(|x| x.is_none())
    .map(|xs| xs.into_iter().flatten().sum())
    .for_each(|x| heap.push(x));
  heap
}