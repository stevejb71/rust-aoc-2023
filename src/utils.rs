use std::{
  fs::File,
  io::{BufRead, BufReader},
  path::PathBuf
};

pub fn read_day_vec(day: usize) -> Vec<String> {
  read_file_vec(day, "day")
}

pub fn read_test_vec(day: usize) -> Vec<String> {
  read_file_vec(day, "test")
}

fn read_file_vec(day: usize, file: &str) -> Vec<String> {
  let file_path = PathBuf::from(format!("data/{}{}.txt", file, day));
  let file = File::open(file_path).expect("no such file");
  let buf = BufReader::new(file);
  buf.lines()
    .map(|l| l.expect("Could not parse line"))
    .collect()
}

