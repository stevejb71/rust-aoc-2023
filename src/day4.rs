pub fn ex1(lines: &Vec<String>) -> usize {
  run(lines, |s1, s2| s1.0 >= s2.0 && s1.1 <= s2.1 || s2.0 >= s1.0 && s2.1 <= s1.1)
}

pub fn ex2(lines: &Vec<String>) -> usize {
  run(lines, |s1, s2| s2.0 <= s1.0 && s1.0 <= s2.1 || s1.0 <= s2.0 && s2.0 <= s1.1)
}

fn run(lines: &Vec<String>, overlap: fn(&Section, &Section) -> bool) -> usize {
  lines.iter()
    .map(|s| parse_sections(&s))
    .filter(|(s1, s2)| overlap(s1, s2))
    .count()
}

type Section = (usize, usize);

fn parse_sections(s: &str) -> (Section, Section) {
  let sections = s.split(|c| c == ',' || c == '-')
    .map(|x| x.parse::<usize>().unwrap())
    .collect::<Vec<_>>();
  ((sections[0], sections[1]), (sections[2], sections[3]))
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::utils::read_test_vec;

  #[test]
  fn test_ex1() {
    let input = read_test_vec(4);
    assert_eq!(2, ex1(&input));
  }

  #[test]
  fn test_ex2() {
    let input = read_test_vec(4);
    assert_eq!(4, ex2(&input));
  }
}