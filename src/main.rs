use std::env;

use aoc_2023::day1;
use aoc_2023::day2;
use aoc_2023::day3;
use aoc_2023::day4;
use aoc_2023::day5;
use aoc_2023::day6;
use aoc_2023::day7;
use aoc_2023::day8;
use aoc_2023::utils::read_day_vec;

fn main() {
  let args: Vec<String> = env::args().collect();
  let day = args.get(1).unwrap().parse::<usize>().unwrap();
  let ex = args.get(2).unwrap().to_owned().to_ascii_uppercase();
  let choice = format!("{}{}", day, ex);
  println!("Running exercise {}...", choice);
  let lines = read_day_vec(day);
  let result = match choice.as_str() {
    "1A" => day1::ex1(&lines).to_string(),
    "1B" => day1::ex2(&lines).to_string(),
    "2A" => day2::ex1(&lines).to_string(),
    "2B" => day2::ex2(&lines).to_string(),
    "3A" => day3::ex1(&lines).to_string(),
    "3B" => day3::ex2(&lines).to_string(),
    "4A" => day4::ex1(&lines).to_string(),
    "4B" => day4::ex2(&lines).to_string(),
    "5A" => day5::ex1(&lines),
    "5B" => day5::ex2(&lines),
    "6A" => day6::ex1(&lines[0]).to_string(),
    "6B" => day6::ex2(&lines[0]).to_string(),
    "7A" => day7::ex1(&lines).to_string(),
    "7B" => day7::ex2(&lines).to_string(),
    "8A" => day8::ex1(&lines).to_string(),
    "8B" => day8::ex2(&lines).to_string(),
    _ => panic!("{}", ex)
  };
  println!("{}", result);
}
