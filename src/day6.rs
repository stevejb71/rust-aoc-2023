use std::collections::HashSet;

pub fn ex1(input: &str) -> usize {
  run(input, 4)
}

pub fn ex2(input: &str) -> usize {
  run(input, 14)
}

fn run(input: &str, size: usize) -> usize {
  let input = input.chars().collect::<Vec<_>>();
  input.windows(size)
    .enumerate()
    .find(|(_, cs)| all_different(cs.to_vec()))
    .unwrap()
    .0 + size
}

fn all_different(cs: Vec<char>) -> bool {
  let len = cs.len();
  HashSet::<char>::from_iter(cs.into_iter()).len() == len
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_ex1_1() {
    assert_eq!(7, ex1("mjqjpqmgbljsphdztnvjfqwrcgsmlb"));
  }

  #[test]
  fn test_ex1_2() {
    assert_eq!(19, ex2("mjqjpqmgbljsphdztnvjfqwrcgsmlb"));
  }
}