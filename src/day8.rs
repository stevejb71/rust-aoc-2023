use std::collections::HashSet;

pub fn ex1(lines: &Vec<String>) -> usize {
  let mut visible_positions = HashSet::new();
  let grid = parse_grid(lines);
  look_from_left(&grid, &mut visible_positions);
  look_from_right(&grid, &mut visible_positions);
  look_from_top(&grid, &mut visible_positions);
  look_from_bottom(&grid, &mut visible_positions);
  visible_positions.len()
}

pub fn ex2(lines: &Vec<String>) -> usize {
  0
}

fn parse_grid(lines: &Vec<String>) -> Vec<Vec<u8>> {
  lines.iter()
    .map(|line| line.chars()
      .map(|c| c.to_digit(10).unwrap() as u8)
      .collect())
    .collect()
}

fn look_from_left(grid: &Vec<Vec<u8>>, visible_positions: &mut HashSet<(usize, usize)>) {
  for y in 0..grid.len() {
    let mut max_height = -1;
    for x in 0..grid[y].len() {
      let height = grid[y][x] as i8;
      if height > max_height {
        visible_positions.insert((x, y));
        max_height = height;
      }
    }
  } 
}

fn look_from_top(grid: &Vec<Vec<u8>>, visible_positions: &mut HashSet<(usize, usize)>) {
  for x in 0..grid.len() {
    let mut max_height = -1;
    for y in 0..grid.len() {
      let height = grid[y][x] as i8;
      if height > max_height {
        visible_positions.insert((x, y));
        max_height = height;
      }
    }
  } 
}

fn look_from_right(grid: &Vec<Vec<u8>>, visible_positions: &mut HashSet<(usize, usize)>) {
  for y in 0..grid.len() {
    let mut max_height = -1;
    for x in (0..grid[y].len()).rev() {
      let height = grid[y][x] as i8;
      if height > max_height {
        visible_positions.insert((x, y));
        max_height = height;
      }
    }
  } 
}

fn look_from_bottom(grid: &Vec<Vec<u8>>, visible_positions: &mut HashSet<(usize, usize)>) {
  for x in 0..grid.len() {
    let mut max_height = -1;
    for y in (0..grid.len()).rev() {
      let height = grid[y][x] as i8;
      if height > max_height {
        visible_positions.insert((x, y));
        max_height = height;
      }
    }
  } 
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::utils::read_test_vec;

  #[test]
  fn test_parse_grid() {
    let lines = vec![
      "123".to_string(),
      "456".to_string(),
      "789".to_string(),
    ];
    let grid = parse_grid(&lines);
    assert_eq!(grid, vec![
      vec![1, 2, 3],
      vec![4, 5, 6],
      vec![7, 8, 9],
    ]);
  }

  #[test]
  fn test_ex1() {
    let lines = read_test_vec(8);
    assert_eq!(21, ex1(&lines));
  }
}