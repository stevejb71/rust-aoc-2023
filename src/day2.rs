pub fn ex1(lines: &Vec<String>) -> usize {
  fn score(line: &str) -> usize {
    match line {
      "A X" => 1 + 3,
      "A Y" => 2 + 6,
      "A Z" => 3 + 0,
      "B X" => 1 + 0,
      "B Y" => 2 + 3,
      "B Z" => 3 + 6,
      "C X" => 1 + 6,
      "C Y" => 2 + 0,
      "C Z" => 3 + 3,
      _ => panic!(),
    }
  }
  run(lines, score)
}

pub fn ex2(lines: &Vec<String>) -> usize {
  fn score(line: &str) -> usize {
    match line {
      "A X" => 0 + 3,
      "B X" => 0 + 1,
      "C X" => 0 + 2,
      "A Y" => 3 + 1,
      "B Y" => 3 + 2,
      "C Y" => 3 + 3,
      "A Z" => 6 + 2,
      "B Z" => 6 + 3,
      "C Z" => 6 + 1,
      _ => panic!(),
    }
  }
  run(lines, score)
}

fn run(lines: &Vec<String>, score: fn(&str) -> usize) -> usize {
  lines.iter()
    .map(|l| l.as_str())
    .map(score)
    .sum::<usize>()
}