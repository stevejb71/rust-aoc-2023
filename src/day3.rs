use std::collections::HashSet;

pub fn ex1(lines: &Vec<String>) -> usize {
  lines.iter()
    .map(|l| split(l))
    .map(|(l, r)| {
      let l = l.chars().collect::<HashSet<_>>();
      let r = r.chars().collect::<HashSet<_>>();
      find_common_letter(&[l, r])
    })
    .map(score)
    .sum::<usize>()
}

pub fn ex2(lines: &Vec<String>) -> usize {
  lines.iter()
    .map(|l| l.chars().collect::<HashSet<_>>())
    .collect::<Vec<_>>()
    .chunks(3)
    .map(find_common_letter)
    .map(score)
    .sum::<usize>()
}

fn find_common_letter(chunk: &[HashSet<char>]) -> char {
  chunk.iter().cloned()
    .reduce(|acc, letter_set| acc.intersection(&letter_set).cloned().collect::<HashSet<_>>())
    .unwrap()
    .iter()
    .cloned()
    .next()
    .unwrap()
} 

fn score(c: char) -> usize {
  if c <= 'Z' {
    (c as usize) - 65 + 27
  } else {
    (c as usize) - 97 + 1
  }
}

fn split<'a>(s: &'a str) -> (&'a str, &'a str) {
  let len = s.len();
  (&s[..len / 2], &s[len / 2..])
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::utils::read_test_vec;

  #[test]
  fn ex1_example() {
    let input = read_test_vec(3);
    assert_eq!(157, ex1(&input))
  }

  #[test]
  fn ex2_example() {
    let input = read_test_vec(3);
    assert_eq!(70, ex2(&input))
  }
}