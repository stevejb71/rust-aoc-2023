use std::slice::Iter;
use nom::{IResult, bytes::complete::{tag, take_while1}, combinator::map_res};

pub fn ex1(lines: &Vec<String>) -> String {
  run(lines, false)
}

pub fn ex2(lines: &Vec<String>) -> String {
  run(lines, true)
}

fn run(lines: &Vec<String>, move_in_bulk: bool) -> String {
  let (mut crates, moves) = parse_all(lines);
  for mv in moves {
    crates.move_crates(mv.amount, mv.from, mv.to, move_in_bulk);
  }
  crates.piles.iter()
    .map(|p| {
      let index = Crates::find_top_index(p) - 1;
      p[index]
    })
    .collect::<String>()

}

#[derive(Debug)]
struct Crates {
  piles: Vec<Vec<char>>
}

#[derive(Debug, PartialEq)]
struct Move {
  amount: u16,
  from: u16,
  to: u16
}

fn parse_u16(input: &str) -> IResult<&str, u16> {
  map_res(take_while1(|c: char| c.is_numeric()), |s: &str| s.parse::<u16>())(input)
}

fn parse_move(input: &str) -> IResult<&str, Move>  {
  let (input, _) = tag("move ")(input)?;
  let (input, amount) = parse_u16(input)?;
  let (input, _) = tag(" from ")(input)?;
  let (input, from) = parse_u16(input)?;
  let (input, _) = tag(" to ")(input)?;
  let (input, to) = parse_u16(input)?;
  Ok((input, Move { amount, from, to }))
}

impl Crates {
  fn new(num_crates: usize) -> Self {
    Crates { piles: vec![vec![]; num_crates] }
  }

  fn move_crates(&mut self, count: u16, from: u16, to: u16, move_in_bulk: bool) {
    let count = count as usize;
    let from = from as usize - 1;
    let to = to as usize - 1;
    let top_from = Self::find_top_index(&self.piles[from]) - 1;
    let top_to = Self::find_top_index(&self.piles[to]);
    if top_to + count >= self.piles[to].len() {
      self.piles[to].resize(top_to + count, ' ');
    }
    if move_in_bulk {
      for i in 0..count {
        let from_index = top_from - i;
        self.piles[to][top_to + count - i - 1] = self.piles[from][from_index];
        self.piles[from][from_index] = ' ';
      }
    } else {
      for i in 0..count {
        let from_index = top_from - i;
        self.piles[to][top_to + i] = self.piles[from][from_index];
        self.piles[from][from_index] = ' ';
      }  
    }
  }
  
  pub fn find_top_index(pile: &Vec<char>) -> usize {
    pile.iter().enumerate()
      .find(|x| *x.1 == ' ')
      .map_or_else(|| pile.len(), |x| x.0)
  }
}

fn parse_all(lines: &Vec<String>) -> (Crates, Vec<Move>) {
  let num_crates = 1 + lines[0].len() / 4;
  let mut lines = lines.iter();
  let crates = parse_crates(num_crates, &mut lines);
  let mut moves = vec![];
  lines.next();
  while let Some(line) = lines.next() {
    let move_result = parse_move(&line).unwrap().1;
    moves.push(move_result);
  }
  (crates, moves)
}

fn parse_crates(num_crates: usize, lines: &mut Iter<String>) -> Crates {
  let mut crates = Crates::new(num_crates);
  while let Some(line) = lines.next() {
    let line = line.chars().collect::<Vec<_>>();
    if line[1] == '1' {
      break;
    }
    for col in 0..num_crates {
      crates.piles[col].push(line[1 + col * 4]);
    }
  }
  crates.piles.iter_mut().for_each(|p| p.reverse());
  crates
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::utils::read_test_vec;

  #[test]
  fn parse_crates_works() {
    let input = read_test_vec(5);
    let crates = parse_crates(3, &mut input.iter());
    assert_eq!(vec![vec!['Z','N',' '], vec!['M','C','D'], vec!['P',' ',' ']], crates.piles);
  }

  #[test]
  fn parse_move_works() {
    let input = "move 5 from 1 to 3";
    let move_result = parse_move(input).unwrap().1;
    assert_eq!(Move {amount: 5, from: 1, to: 3}, move_result);
  }

  #[test]
  fn test_parse_all() {
    let input = read_test_vec(5);
    let (crates, moves) = parse_all(&input);
    assert_eq!(vec![vec!['Z','N',' '], vec!['M','C','D'], vec!['P',' ', ' ']], crates.piles);
    assert_eq!(vec![Move {amount: 1, from: 2, to: 1}, Move {amount: 3, from: 1, to: 3}, Move {amount: 2, from: 2, to: 1}, Move {amount: 1, from: 1, to: 2}], moves);
  }

  #[test]
  fn test_move_crates_one_at_a_time() {
    let mut crates = Crates::new(3);
    crates.piles = vec![vec!['Z','N',' '], vec!['M','C','D'], vec!['P',' ', ' ']];
    crates.move_crates(1, 2, 1, false);
    assert_eq!(vec![vec!['Z','N','D'], vec!['M','C',' '], vec!['P',' ', ' ']], crates.piles);
    crates.move_crates(3, 1, 3, false);
    assert_eq!(vec![vec![' ',' ',' '], vec!['M','C',' '], vec!['P','D','N', 'Z']], crates.piles);
  } 

  #[test]
  fn test_ex1() {
    let input = read_test_vec(5);
    assert_eq!("CMZ", ex1(&input));
  }

  #[test]
  fn test_ex2() {
    let input = read_test_vec(5);
    assert_eq!("MCD", ex2(&input));
  }
}