use std::collections::HashMap;
use std::slice::Iter;

pub fn ex1(lines: &Vec<String>) -> usize {
  let mut builder = DirectoryBuilder::new();
  builder.parse(&mut lines.iter());
  let totals = builder.aggregate();
  totals.iter()
    .map(|(_, &size)| size as usize)
    .filter(|&size| size <= 100000)
    .sum::<usize>()
}

pub fn ex2(lines: &Vec<String>) -> usize {
  let mut builder = DirectoryBuilder::new();
  builder.parse(&mut lines.iter());
  let totals = builder.aggregate();
  let used_size = totals["/"];
  let to_free_up = used_size - 40000000;
  let dir_size_to_free = totals.values()
    .filter(|&&size| size >= to_free_up)
    .min()
    .unwrap();
  *dir_size_to_free as usize
}

// struct called DirectoryBuilder with current directory as Vec<String> and HashMap of directories to u64
#[derive(Debug)]
struct DirectoryBuilder {
  current_directory: Vec<String>,
  directories: HashMap<Vec<String>, u64>,
}

impl DirectoryBuilder {
  // new function that returns a new DirectoryBuilder with current being "/"
  fn new() -> DirectoryBuilder {
    DirectoryBuilder {
      current_directory: vec!["/".to_string()],
      directories: HashMap::new(),
    }
  }

  pub fn aggregate(&self) -> HashMap<String, u64> {
    let mut aggregate = HashMap::new();
    let mut directories = Vec::from_iter(self.directories.iter());
    directories.sort_by_key(|(k, _)| 1000000 - k.len());
    for (dir, size) in directories {
      let mut dir = dir.clone();
      while !dir.is_empty() {
        let parent_size = aggregate.entry(dir.join("")).or_insert(0u64);
        *parent_size += size;
        dir.pop();
      }
    }
    aggregate
  }

  pub fn parse(&mut self, lines: &mut Iter<String>) {
    while let Some(line) = lines.next() {
      // if line starts with "$ cd" then cd to text after cd
      if line.starts_with("$ cd") {
        self.cd(&line[5..]);
      } 
      // else if line starts with number then call add_file with number and dir_folder
      else if line.chars().next().unwrap().is_numeric() {
        let mut line_iter = line.split_whitespace();
        let size = line_iter.next().unwrap().parse::<u64>().unwrap();
        self.add_file(size);
      }
    }
  }

  // cd function that changes the current directory
  fn cd(&mut self, path: &str) {
    // if path is "/", set current directory to "/"
    if path == "/" {
      self.current_directory = vec!["/".to_string()];
    } else {
      // split path by "/"
      path.split("/").for_each(|elt| {
        if elt == ".." {
          self.current_directory.pop();
        } else {
          self.current_directory.push(elt.to_string());
        }
      });
    }
  }

  // add file that adds a file with size at the current directory to the map
  fn add_file(&mut self, size: u64) {
    let directory = self.current_directory.clone();
    let current_size = self.directories.entry(directory).or_insert(0);
    *current_size += size;
  }
}


#[cfg(test)]
mod test {
  use crate::utils::read_test_vec;
  use super::*;

  #[test]
  fn cd_to_relative_path() {
    let mut builder = DirectoryBuilder::new();
    builder.cd("a/b/c/d");
    assert_eq!(builder.current_directory, vec!["/", "a", "b", "c", "d"]);
  }

  #[test]
  fn cd_back() {
    let mut builder = DirectoryBuilder::new();
    builder.cd("a/b/c/d");
    builder.cd("../..");
    assert_eq!(builder.current_directory, vec!["/", "a", "b"]);
  }

  #[test]
  fn add_files() {
    let mut builder = DirectoryBuilder::new();
    builder.cd("a/b");
    builder.add_file(10);
    builder.add_file(20);
    assert_eq!(builder.directories[&to_strings(vec!["/", "a", "b"])], 30);
  }

  #[test]
  fn parse_cd_line() {
    let mut builder = DirectoryBuilder::new();
    builder.parse(&mut to_strings(vec!["$ cd a/b", "$ cd c/d"]).iter());
    assert_eq!(builder.current_directory, vec!["/", "a", "b", "c", "d"]);
  }

  #[test]
  fn parse_dir_lines() {
    let mut builder = DirectoryBuilder::new();
    builder.parse(&mut to_strings(vec![
      "$ cd a", 
      "$ ls",
      "8000 w.txt",
      "$ cd c", 
      "$ ls", 
      "1000 x.txt", 
      "dir z", 
      "2000 y.txt"]
    ).iter());
    // build HashMap with values "/a/c" to 3000
    let mut expected = HashMap::new();
    expected.insert(to_strings(vec!["/", "a"]), 8000);
    expected.insert(to_strings(vec!["/", "a", "c"]), 3000);
    assert_eq!(builder.directories, expected);
  }

  #[test]
  fn test_aggregate() {
    let mut builder = DirectoryBuilder::new();
    builder.parse(&mut to_strings(vec![
      "$ cd a", 
      "$ ls",
      "8000 w.txt",
      "$ cd c", 
      "$ ls", 
      "1000 x.txt", 
      "dir z", 
      "2000 y.txt"]
    ).iter());
    let mut expected = HashMap::new();
    expected.insert("/".to_string(), 11000);
    expected.insert("/a".to_string(), 11000);
    expected.insert("/ac".to_string(), 3000);
    assert_eq!(builder.aggregate(), expected);
  }

  #[test]
  fn test_ex1() {
    assert_eq!(95437, ex1(&read_test_vec(7)));
  }

  #[test]
  fn test_ex2() {
    assert_eq!(24933642, ex2(&read_test_vec(7)));
  }

  fn to_strings(v: Vec<&str>) -> Vec<String> {
    v.iter().map(|s| s.to_string()).collect()
  }
}
